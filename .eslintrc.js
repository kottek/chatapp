module.exports = {
  extends: 'airbnb',
  parser: 'babel-eslint',
  plugins: ['react'],
  rules: {
    semi: ['error', 'never'],
  },
  globals: {
    React: true,
    mount: true,
    render: true,
    shallow: true,
  },
  env: {
    browser: true,
    jest: true,
  },
}
