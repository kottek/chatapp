## Blocket chat app

## Use Guide

First off, clone the repository and then install node modules

```
git clone
cd blocketchatapp
npm install
```

### Setting up MongoDB

I used MongoDB for storing messages and users locally.
Once you've installed MongoDB start up the MongoDB server in a new terminal with the following commands:

```
mkdir db
mongod --dbpath=./db --smallfiles
```

Then open a new terminal and type in `mongo` and type in `use chat_dev`
This is your database interface. You can query the database for records for example: `db.users.find()` or `db.stats()`. If you want to remove all channels for example you can type `db.channels.remove({})`.

Now that you've done all that, you can go go ahead and code away!

### Development

```
npm run dev
```

And then point your browser to `localhost:3000`

You can create channels with the + sign on the nav bar on the left.
If you click on a user's name to send him a private message (opens a private channel)

Note:
This program comes with [redux-dev tools](https://github.com/gaearon/redux-devtools)

- To SHOW or HIDE the dev tool panel press ctrl+h
- To change position press ctrl+m

### Production

```
npm run build
npm start
```

And then point your browser to `localhost:3000`

## Todos

- Token/oAuth
- persistancy
- implement reselect
- implement async-props
- UI, Theme's
- SSR
- Notifications,
- unread Msgs
- Threads

### Time

- Started 25th/Nov/2018 -> 6:36 AM
